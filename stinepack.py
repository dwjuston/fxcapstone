from numpy import diff,sin,cos,square,sqrt,log,exp,arange,linspace
import numpy as np

def stinemanSlopes(x,y,scale=False):
    m=len(x)
    m1=m-1
    if m==2: return np.array(list(diff(y)/diff(x))*2)
    else:
        if scale:
            sx = max(x)-min(x)
            sy = max(y)-min(y)
            if sy <= 0:
                sy = 1
            x = x/sx
            y = y/sy
        dx,dy=diff(x),diff(y)
        yp=np.zeros(m)
        dx2dy2p = dx[1:]**2 + dy[1:]**2
        dx2dy2m = dx[:-1]**2 + dy[:-1]**2
        yp[1:-1] =(dy[:-1] * dx2dy2p + dy[1:] * dx2dy2m)/(dx[:-1] * dx2dy2p + dx[1:] * dx2dy2m)
        s = dy[0]/dx[0]
        if (s >= 0 and s >= yp[1]) or (s <= 0 and s <= yp[1]):
            yp[0] = 2 * s - yp[1]
        else:
            yp[0] = s + abs(s) * (s - yp[1])/(abs(s) + abs(s - yp[1]))
        s = dy[-1]/dx[-1]
        if (s >= 0 and s >= yp[-2]) or (s <= 0 and s <= yp[-2]):
            yp[-1] = 2 * s - yp[-2]
        else:
            yp[-1] = s + abs(s) * (s - yp[-2])/(abs(s) + abs(s - yp[-2]))
        
        if scale:
            yp = yp * sy/sx
        return yp
    
def stinterp(x,y,xout,method="scaledstineman"):
    '''
    if (missing(x) || missing(y) || missing(xout)) 
        stop("Wrong number of input arguments, x, y and xout must be specified")
    if (!is.vector(x) || !is.vector(y) || !is.vector(xout) || 
        !is.numeric(x) || !is.numeric(y) || !is.numeric(xout)) 
        stop("x, y and xout must be numeric vectors")
    if (length(x) < 2) 
        stop("x must have 2 or more elements")
    if (length(x) != length(y)) 
        stop("x must have the same number of elements as y")
    if (any(is.na(x)) || any(is.na(x)) || any(is.na(xout))) 
        stop("NAs in x, y or xout are not allowed")
    if (!missing(yp)) {
        if (!is.vector(yp) || !is.numeric(yp)) 
            stop("yp must be a numeric vector")
        if (length(y) != length(yp)) 
            stop("When specified, yp must have the same number of elements as y")
        if (any(is.na(yp))) 
            stop("NAs in yp are not allowed")
        if (!missing(method)) 
            stop("Method should not be specified if yp is given")
    }
    '''
    dx = diff(x)
    dy = diff(y)
    #if (np.any(dx <= 0)) 
    #    stop("The values of x must strictly increasing")
    if method=="scaledstineman":
        yp = stinemanSlopes(x, y,scale=True)
    else:
        yp = stinemanSlopes(x, y,scale=False)
        
    m = len(x)
    m1 = m - 2
    s = dy/dx
    k = len(xout)
    ix = np.array([np.searchsorted(x,target)-1 for target in xout])
    epx = 5 * 1e-8 * (max(x)-min(x))
    ix[(min(x) - epx <= xout) & (xout <= min(x))]=0
    ix[(max(x) <= xout) & (xout <= max(x) + epx)]=m1
    idx = (ix >= 0) & (ix <= m1)
    ix1 = ix[idx]
    ix2 = ix1 + 1
    dxo1 = xout[idx] - x[ix1]
    dxo2 = xout[idx] - x[ix2]
    y0o = y[ix1] + s[ix1] * dxo1
    dyo1 = (yp[ix1] - s[ix1]) * dxo1
    dyo2 = (yp[ix2] - s[ix1]) * dxo2
    dyo1dyo2 = dyo1 * dyo2
    yo = y0o

    if m>2: 
        mask = dyo1dyo2 > 0
        yo[mask] = y0o[mask] + dyo1dyo2[mask]/(dyo1[mask] + dyo2[mask])
        mask = dyo1dyo2 < 0
        yo[mask] = y0o[mask] + dyo1dyo2[mask] * (dxo1[mask] + dxo2[mask])/(dyo1[mask] - dyo2[mask])/((dx[ix1])[mask])

    yout = np.empty(k)
    yout[:] = np.nan
    yout[idx] = yo
    return {
        "x":xout,
        "y":yout
    }

