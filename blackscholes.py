#!/usr/bin/env python
# coding: utf-8

# In[6]:


import numpy as np
from scipy.stats import norm
from math import pi
from numpy import exp, sqrt, log, tanh


# In[7]:


# ----------- BSM -----------------
pnorm = lambda x : norm.cdf(x)
    
def BSFormula(S0, K, T, r, sigma):
    x = log(S0/K)+r*T;
    sig = sigma*sqrt(T);
    d1 = x/sig+sig/2;
    d2 = d1 - sig;
    pv = exp(-r*T);
    return S0*pnorm(d1) - pv*K*pnorm(d2)

def BSFormulaPut(S0, K, T, r, sigma):
    x = log(S0/K) + r * T
    sig = sigma * sqrt(T)
    d1 = x/sig + sig/2
    d2 = d1 - sig
    pv = exp(-r * T)
    return pv * K * pnorm(-d2) - S0 * pnorm(-d1)

# This function now works with vectors of strikes and option values
def BSImpliedVolCall(S0, K, T, r, C):
    nK = len(K);
    sigmaL = np.array([1e-10]*nK)
    CL = BSFormula(S0, K, T, r, sigmaL);
    sigmaH = np.array([10]*nK)
    CH = BSFormula(S0, K, T, r, sigmaH);
    while np.mean(sigmaH - sigmaL) > 1e-10:
        sigma = (sigmaL + sigmaH)/2;
        CM = BSFormula(S0, K, T, r, sigma);
        CL = CL + (CM < C)*(CM-CL);
        sigmaL = sigmaL + (CM < C)*(sigma-sigmaL);
        CH = CH + (CM >= C)*(CM-CH);
        sigmaH = sigmaH + (CM >= C)*(sigma-sigmaH);
    return sigma

# This function also works with vectors of strikes and option values  
def BSImpliedVolPut(S0, K, T, r, P): 
    nK = len(K)
    sigmaL = np.array([1e-10]*nK)
    PL = BSFormulaPut(S0, K, T, r, sigmaL)
    sigmaH = np.array([10]*nK)
    PH = BSFormulaPut(S0, K, T, r, sigmaH)
    while np.mean(sigmaH - sigmaL) > 1e-10:
        sigma = (sigmaL + sigmaH)/2
        PM = BSFormulaPut(S0, K, T, r, sigma)
        PL = PL + (PM < P) * (PM - PL)
        sigmaL = sigmaL + (PM < P) * (sigma - sigmaL)
        PH = PH + (PM >= P) * (PM - PH)
        sigmaH = sigmaH + (PM >= P) * (sigma - sigmaH)
    return sigma

def BSImpliedVolOTM(S0, K, T, r, V):
    f = S0 * exp(r*T)
    if not isinstance(K, np.ndarray): K=np.array([K])
    if K>f: return BSImpliedVolCall(S0, K, T, r, V)
    else: return BSImpliedVolPut(S0, K, T, r, V)


# In[14]:


'''
vol=BSImpliedVolOTM(S0=1, K=1, T=1, r=0, V=0.09701405804263008)
print('BSImpliedVolOTM',vol)

print("\nBSM")
vols = np.array([0.23,0.20,0.18])
K = np.array([0.9,1.0,1.1]) 
optVals = BSFormula(S0=1,K=K,T=1,r=0,sigma=vols)
print(optVals)
optVals2 = BSFormulaPut(S0=1,K=K,T=1,r=0,sigma=vols)
print(optVals2)
impVols = BSImpliedVolCall(S0=1, K=K, T=1, r=0, C=optVals)
print(impVols)
impVols2 = BSImpliedVolPut(S0=1, K=K, T=1, r=0, P=optVals2)
print(impVols2)
'''

