#!/usr/bin/env python
# coding: utf-8

# In[2]:


from scipy.sparse import diags
import pandas as pd
import numpy as np

def makeOp_1C(N,h):
    # make finite difference matrix operator
    # 1st order derivative, Central: [f(x+h)-f(x-h)]/(2h)
    A=diags([-1/2/h,0,1/2/h], [-1, 0, 1], [N, N]).toarray()
    A[0,0]=-1/h; A[0,1]=1/h # at start, forward, (f(x+h)-f(x))/h
    A[-1,-2]=-1/h; A[-1,-1]=1/h # at the end, backward, (f(x)-f(x-h))/h
    return A

def makeOp_2C(N,h):
    # make finite difference matrix operator
    # 1st order derivative, Central: [f(x+h)-f(x-h)]/(2h)
    A=diags([1/h**2,-2/h**2,1/h**2], [-1, 0, 1], [N, N]).toarray()
    A[0,0]=0; A[0,1]=0 # Second order boundary, all 0
    A[-1,-2]=0; A[-1,-1]=0
    return A

def do_finite_difference(U,logstrikes,targettimes):
    h=logstrikes[1]-logstrikes[0]
    N=len(logstrikes)
    A=makeOp_1C(N=N,h=h)
    dUdk=A@U.T; dUdk=dUdk.T
    
    h=targettimes[1]-targettimes[0]
    N=len(targettimes)
    A=makeOp_1C(N=N,h=h)
    dUdT=A@U
    
    h=logstrikes[1]-logstrikes[0]
    N=len(logstrikes)
    A=makeOp_2C(N=N,h=h)
    d2Udk2=A@U.T; d2Udk2=d2Udk2.T
    
    return U,dUdk,d2Udk2,dUdT

class Dupire:
    def __init__(self,U,logstrikes,targettimes):
        df=pd.Series(logstrikes,name="value").reset_index()
        df.index=df['value']
        self.kDict=df['index'].to_dict()
        
        df=pd.Series(targettimes,name="value").reset_index()
        df.index=df['value']
        self.tDict=df['index'].to_dict()
        
        self.U, self.dUdk, self.d2Udk2, self.dUdT=do_finite_difference(U,logstrikes,targettimes)
        self.logstrikes, self.targettimes = logstrikes,targettimes
        self.getMat()
        
    def w(self,i,j):
        return self.U[i,j]
        
    def dwdk(self,i,j):
        return self.dUdk[i,j]

    def d2wdk2(self,i,j):
        return self.d2Udk2[i,j]

    def dwdt(self,i,j):
        return self.dUdT[i,j]
    
    def g(self,k,i,j):
        w0=self.w(i,j)
        w1=self.dwdk(i,j)
        w2=self.d2wdk2(i,j)
        return (1.-0.5*k*w1/w0)**2 - 0.25*w1**2 *(0.25 + 1./w0) + 0.5*w2
    
    def LocalVar(self,k,t):
        i=self.tDict[t]
        j=self.kDict[k]
        return self.dwdt(i,j)/self.g(k,i,j)
    
    def getMat(self):
        self.mat=np.array([[self.LocalVar(k,t) for k in self.logstrikes] for t in self.targettimes])
        self.mat=pd.DataFrame(self.mat, columns=self.logstrikes, index=self.targettimes)
        return self.mat
    
    def get(self,t,k):
        df=self.mat
        t2=df.index.searchsorted(t); 
        if t2!=0: 
            t1=t2-1
        else: 
            t1=0
        k2=df.columns.searchsorted(k); 
        if k2!=0:
            k1=k2-1
        else:
            k1=0
        return (df.iloc[t1,k1] + df.iloc[t1,k2] + df.iloc[t2,k1] + df.iloc[t2,k2])/4.0


def LV_linterp(df, t, k):
    t2=df.index.searchsorted(t); t1=t2-1
    k2=df.columns.searchsorted(k); k1=k2-1
    return (df.iloc[t1,k1] + df.iloc[t1,k2] + df.iloc[t2,k1] + df.iloc[t2,k2])/4.0




