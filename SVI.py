import pandas as pd
import scipy
import cmath 
from math import pi
import numpy as np

from numpy import exp,sqrt,log,sign,tanh,square,subtract,mean,unique,empty,nansum,isnan,vectorize
from copy import deepcopy
from matplotlib import pyplot as plt
from scipy.integrate import quad
from scipy.stats import norm
from scipy.optimize import least_squares, minimize, rosen, Bounds
from scipy.sparse import diags

from stinepack import *


class SVIPARAMS:
    def __init__(self, a,b,sig,rho,m):
        self.a=a
        self.b=b
        self.sig=sig
        self.rho=rho
        self.m=m
        
    @classmethod
    def load(self,pdSeries):
        a=pdSeries['a']
        b=pdSeries['b']
        sig=pdSeries['sig']
        rho=pdSeries['rho']
        m=pdSeries['m']
        return SVIPARAMS(a,b,sig,rho,m)
    
    def to_list(self):
        return [self.a,self.b,self.sig,self.rho,self.m]

    def to_numpy(self):
        return np.array(self.to_list())
    
class JWPARAMS:
    def __init__(self,vt,psit,pt,ct,varmint,texp):
        self.vt=vt
        self.psit=psit
        self.pt=pt
        self.ct=ct
        self.varmint=varmint
        self.texp=texp
    
    @classmethod
    def load(self,pdSeries):
        vt=pdSeries['vt']
        psit=pdSeries['psit']
        pt=pdSeries['pt']
        ct=pdSeries['ct']
        varmint=pdSeries['varmint']
        texp=pdSeries['texp']
        return JWPARAMS(vt,psit,pt,ct,varmint,texp)
    
    def to_list(self):
        return [self.vt,self.psit,self.pt,self.ct,self.varmint,self.texp]
    
    def to_numpy(self):
        return np.array(self.to_list())
    
    
def SVI(sviparams,k):
    a,b,sig,rho,m=[np.nan]*5
    if isinstance(sviparams, pd.Series) or isinstance(sviparams, SVIPARAMS):
        a=sviparams.a
        b=sviparams.b
        sig=sviparams.sig
        rho=sviparams.rho
        m=sviparams.m
    elif isinstance(sviparams, np.ndarray) or isinstance(sviparams, list):
        a,b,sig,rho,m=sviparams
    return a + b *(rho*(k-m)+ sqrt((k-m)*(k-m) + sig*sig))

def SVI_toJW(sviparams,texp):
    a=sviparams.a
    b=sviparams.b
    sig=sviparams.sig
    rho=sviparams.rho
    m=sviparams.m
    
    vt = (a+b*(-rho*m+sqrt(m**2+sig**2)))/texp; # Recall that svi parameters a and b are in w terms
    bhat = sqrt(1/(vt*texp))*b;
    psit = bhat/2*(-m/sqrt(m**2+sig**2)+rho);
    pt = bhat*(1-rho);
    ct = bhat*(1+rho);
    varmint = (a+b*abs(sig)*sqrt(1-rho**2))/texp;
    return JWPARAMS(vt,psit,pt,ct,varmint,texp)

def SVI_fromJW(jwparams):
    vt=jwparams.vt
    psit=jwparams.psit
    pt=jwparams.pt
    ct=jwparams.ct
    varmint=jwparams.varmint
    texp=jwparams.texp
    
    sqrtw = sqrt(vt*texp);
    bhat = (pt+ct)/2;
    b = bhat*sqrtw;
    rho = 1- pt/bhat; 
    bet = (rho-2*psit/bhat);
    alpha = sign(bet)*sqrt(1/bet**2-1);
    
    m = (vt-varmint)*texp/b/(-rho+sign(alpha)*sqrt(1+alpha**2)-alpha*sqrt(1-rho**2));
    sig = alpha*m;
    a = varmint*texp - b*sig*sqrt(1-rho**2);
    return SVIPARAMS(a,b,sig,rho,m)

def SVI_ComputeRoots(sviparam1,sviparam2):
    a1,b1,s1,r1,m1,a2,b2,s2,r2,m2=[0]*10
    if (isinstance(sviparam1, pd.Series) and isinstance(sviparam2, pd.Series)) or (isinstance(sviparam1, SVIPARAMS) and isinstance(sviparam2, SVIPARAMS)):
        a1 = sviparam1.a
        b1 = sviparam1.b
        s1 = sviparam1.sig
        r1 = sviparam1.rho 
        m1 = sviparam1.m

        a2 = sviparam2.a
        b2 = sviparam2.b
        r2 = sviparam2.rho
        m2 = sviparam2.m
        s2 = sviparam2.sig
    elif isinstance(sviparam1, np.ndarray) and isinstance(sviparam2, np.ndarray):
        a1,b1,s1,r1,m1=sviparam1
        a2,b2,s2,r2,m2=sviparam2
    else:
        print("Error:input type")
        raise
    
    # The standard form of the quartic is q4 x**4 + q3 x**3 +q2 x**2 +q1 x + q0 == 0
    # Note that multiplying all the coefficients qi by a factor should have no effect on the roots.
    q0 = 1000000 * (a1 ** 4 + a2 ** 4 + b1 ** 4 * m1 ** 4 - 2 * b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 ** 2 + b2 ** 4 * m2 ** 4 - 
            2 * b1 ** 4 * m1 ** 4 * r1 ** 2 - 2 * b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 ** 2 * r1 ** 2 + b1 ** 4 * m1 ** 4 * r1 ** 4 + 
            4 * b1 ** 3 * b2 * m1 ** 3 * m2 * r1 * r2 + 4 * b1 * b2 ** 3 * m1 * m2 ** 3 * r1 * r2 - 
            4 * b1 ** 3 * b2 * m1 ** 3 * m2 * r1 ** 3 * r2 - 2 * b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 ** 2 * r2 ** 2 - 
            2 * b2 ** 4 * m2 ** 4 * r2 ** 2 + 6 * b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 ** 2 * r1 ** 2 * r2 ** 2 - 
            4 * b1 * b2 ** 3 * m1 * m2 ** 3 * r1 * r2 ** 3 + b2 ** 4 * m2 ** 4 * r2 ** 4 + 
            4 * a2 ** 3 * (b1 * m1 * r1 - b2 * m2 * r2) - 4 * a1 ** 3 * (a2 + b1 * m1 * r1 - 
              b2 * m2 * r2) + 2 * b1 ** 4 * m1 ** 2 * s1 ** 2 - 2 * b1 ** 2 * b2 ** 2 * m2 ** 2 * s1 ** 2 - 
            2 * b1 ** 4 * m1 ** 2 * r1 ** 2 * s1 ** 2 + 4 * b1 ** 3 * b2 * m1 * m2 * r1 * r2 * s1 ** 2 - 
            2 * b1 ** 2 * b2 ** 2 * m2 ** 2 * r2 ** 2 * s1 ** 2 + b1 ** 4 * s1 ** 4 - 2 * b1 ** 2 * b2 ** 2 * m1 ** 2 * s2 ** 2 + 
            2 * b2 ** 4 * m2 ** 2 * s2 ** 2 - 2 * b1 ** 2 * b2 ** 2 * m1 ** 2 * r1 ** 2 * s2 ** 2 + 
            4 * b1 * b2 ** 3 * m1 * m2 * r1 * r2 * s2 ** 2 - 2 * b2 ** 4 * m2 ** 2 * r2 ** 2 * s2 ** 2 - 
            2 * b1 ** 2 * b2 ** 2 * s1 ** 2 * s2 ** 2 + b2 ** 4 * s2 ** 4 + 4 * a2 * (b1 * m1 * r1 - b2 * m2 * r2) * 
             (-2 * b1 * b2 * m1 * m2 * r1 * r2 + b1 ** 2 * (m1 ** 2 * (-1 + r1 ** 2) - s1 ** 2) + 
              b2 ** 2 * (m2 ** 2 * (-1 + r2 ** 2) - s2 ** 2)) - 4 * a1 * (a2 + b1 * m1 * r1 - 
              b2 * m2 * r2) * (a2 ** 2 - 2 * b1 * b2 * m1 * m2 * r1 * r2 + 2 * a2 * (b1 * m1 * r1 - 
                b2 * m2 * r2) + b1 ** 2 * (m1 ** 2 * (-1 + r1 ** 2) - s1 ** 2) + 
              b2 ** 2 * (m2 ** 2 * (-1 + r2 ** 2) - s2 ** 2)) + 2 * a2 ** 2 * 
             (-6 * b1 * b2 * m1 * m2 * r1 * r2 + b1 ** 2 * (m1 ** 2 * (-1 + 3 * r1 ** 2) - s1 ** 2) + 
              b2 ** 2 * (m2 ** 2 * (-1 + 3 * r2 ** 2) - s2 ** 2)) + 
            2 * a1 ** 2 * (3 * a2 ** 2 - 6 * b1 * b2 * m1 * m2 * r1 * r2 + 6 * a2 * (b1 * m1 * r1 - 
                b2 * m2 * r2) + b1 ** 2 * (m1 ** 2 * (-1 + 3 * r1 ** 2) - s1 ** 2) + 
              b2 ** 2 * (m2 ** 2 * (-1 + 3 * r2 ** 2) - s2 ** 2)))
        
    q1 = 1000000 * 4 * (-(b1 ** 4 * m1 ** 3) + b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 + b1 ** 2 * b2 ** 2 * m1 * m2 ** 2 - 
            b2 ** 4 * m2 ** 3 + 2 * b1 ** 4 * m1 ** 3 * r1 ** 2 + b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 * r1 ** 2 + 
            b1 ** 2 * b2 ** 2 * m1 * m2 ** 2 * r1 ** 2 - b1 ** 4 * m1 ** 3 * r1 ** 4 - b1 ** 3 * b2 * m1 ** 3 * r1 * r2 - 
            3 * b1 ** 3 * b2 * m1 ** 2 * m2 * r1 * r2 - 3 * b1 * b2 ** 3 * m1 * m2 ** 2 * r1 * r2 - b1 * b2 ** 3 * m2 ** 3 * r1 * r2 + b1 ** 3 * b2 * m1 ** 3 * r1 ** 3 * r2 + 3 * b1 ** 3 * b2 * m1 ** 2 * m2 * 
             r1 ** 3 * r2 + b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 * r2 ** 2 + b1 ** 2 * b2 ** 2 * m1 * m2 ** 2 * r2 ** 2 + 
            2 * b2 ** 4 * m2 ** 3 * r2 ** 2 - 3 * b1 ** 2 * b2 ** 2 * m1 ** 2 * m2 * r1 ** 2 * r2 ** 2 - 
            3 * b1 ** 2 * b2 ** 2 * m1 * m2 ** 2 * r1 ** 2 * r2 ** 2 + 3 * b1 * b2 ** 3 * m1 * m2 ** 2 * r1 * r2 ** 3 + 
            b1 * b2 ** 3 * m2 ** 3 * r1 * r2 ** 3 - b2 ** 4 * m2 ** 3 * r2 ** 4 + a1 ** 3 * (b1 * r1 - b2 * r2) + 
            a2 ** 3 * (-(b1 * r1) + b2 * r2) + a2 ** 2 * (b1 ** 2 * (m1 - 3 * m1 * r1 ** 2) + 3 * b1 * b2 * (m1 + m2) * r1 * r2 + b2 ** 2 * m2 * (1 - 3 * r2 ** 2)) + 
            a1 ** 2 * (b1 ** 2 * (m1 - 3 * m1 * r1 ** 2) + 3 * b1 * r1 * (-a2 + b2 * (m1 + m2) * r2) + 
              b2 * (3 * a2 * r2 + b2 * (m2 - 3 * m2 * r2 ** 2))) - b1 ** 4 * m1 * s1 ** 2 + 
            b1 ** 2 * b2 ** 2 * m2 * s1 ** 2 + b1 ** 4 * m1 * r1 ** 2 * s1 ** 2 - b1 ** 3 * b2 * m1 * r1 * r2 * s1 ** 2 - 
            b1 ** 3 * b2 * m2 * r1 * r2 * s1 ** 2 + b1 ** 2 * b2 ** 2 * m2 * r2 ** 2 * s1 ** 2 + 
            b1 ** 2 * b2 ** 2 * m1 * s2 ** 2 - b2 ** 4 * m2 * s2 ** 2 + b1 ** 2 * b2 ** 2 * m1 * r1 ** 2 * s2 ** 2 - 
            b1 * b2 ** 3 * m1 * r1 * r2 * s2 ** 2 - b1 * b2 ** 3 * m2 * r1 * r2 * s2 ** 2 + 
            b2 ** 4 * m2 * r2 ** 2 * s2 ** 2 + a2 * (b1 ** 2 * b2 * r2 * (m1 ** 2 * (-1 + 3 * r1 ** 2) + 
                2 * m1 * m2 * (-1 + 3 * r1 ** 2) - s1 ** 2) + b1 ** 3 * r1 * (-3 * m1 ** 2 * 
                 (-1 + r1 ** 2) + s1 ** 2) + b2 ** 3 * r2 * (3 * m2 ** 2 * (-1 + r2 ** 2) - s2 ** 2) + 
              b1 * b2 ** 2 * r1 * (m1 * m2 * (2 - 6 * r2 ** 2) + m2 ** 2 * (1 - 3 * r2 ** 2) + s2 ** 2)) + 
            a1 * (3 * a2 ** 2 * (b1 * r1 - b2 * r2) + a2 * (2 * b1 ** 2 * m1 * (-1 + 3 * r1 ** 2) - 
                6 * b1 * b2 * (m1 + m2) * r1 * r2 + 2 * b2 ** 2 * m2 * (-1 + 3 * r2 ** 2)) + 
              b1 ** 3 * r1 * (3 * m1 ** 2 * (-1 + r1 ** 2) - s1 ** 2) + b1 ** 2 * b2 * r2 * ( 
                m1 * m2 * (2 - 6 * r1 ** 2) + m1 ** 2 * (1 - 3 * r1 ** 2) + s1 ** 2) + 
              b1 * b2 ** 2 * r1 * (2 * m1 * m2 * (-1 + 3 * r2 ** 2) + m2 ** 2 * (-1 + 3 * r2 ** 2) - 
                s2 ** 2) + b2 ** 3 * r2 * (-3 * m2 ** 2 * (-1 + r2 ** 2) + s2 ** 2)))
    
    q2 = 1000000 * -2 * (-3 * b1 ** 4 * m1 ** 2 + b1 ** 2 * b2 ** 2 * m1 ** 2 + 4 * b1 ** 2 * b2 ** 2 * m1 * m2 + 
            b1 ** 2 * b2 ** 2 * m2 ** 2 - 3 * b2 ** 4 * m2 ** 2 + 6 * b1 ** 4 * m1 ** 2 * r1 ** 2 + 
            b1 ** 2 * b2 ** 2 * m1 ** 2 * r1 ** 2 + 4 * b1 ** 2 * b2 ** 2 * m1 * m2 * r1 ** 2 + 
            b1 ** 2 * b2 ** 2 * m2 ** 2 * r1 ** 2 - 3 * b1 ** 4 * m1 ** 2 * r1 ** 4 - 6 * b1 ** 3 * b2 * m1 ** 2 * r1 * r2 - 
            6 * b1 ** 3 * b2 * m1 * m2 * r1 * r2 - 6 * b1 * b2 ** 3 * m1 * m2 * r1 * r2 - 
            6 * b1 * b2 ** 3 * m2 ** 2 * r1 * r2 + 6 * b1 ** 3 * b2 * m1 ** 2 * r1 ** 3 * r2 + 
            6 * b1 ** 3 * b2 * m1 * m2 * r1 ** 3 * r2 + b1 ** 2 * b2 ** 2 * m1 ** 2 * r2 ** 2 + 
            4 * b1 ** 2 * b2 ** 2 * m1 * m2 * r2 ** 2 + b1 ** 2 * b2 ** 2 * m2 ** 2 * r2 ** 2 + 6 * b2 ** 4 * m2 ** 2 * r2 ** 2 - 
            3 * b1 ** 2 * b2 ** 2 * m1 ** 2 * r1 ** 2 * r2 ** 2 - 12 * b1 ** 2 * b2 ** 2 * m1 * m2 * r1 ** 2 * r2 ** 2 - 
            3 * b1 ** 2 * b2 ** 2 * m2 ** 2 * r1 ** 2 * r2 ** 2 + 6 * b1 * b2 ** 3 * m1 * m2 * r1 * r2 ** 3 + 
            6 * b1 * b2 ** 3 * m2 ** 2 * r1 * r2 ** 3 - 3 * b2 ** 4 * m2 ** 2 * r2 ** 4 - 
            a1 ** 2 * (b1 ** 2 * (-1 + 3 * r1 ** 2) - 6 * b1 * b2 * r1 * r2 + b2 ** 2 * (-1 + 3 * r2 ** 2)) - 
            a2 ** 2 * (b1 ** 2 * (-1 + 3 * r1 ** 2) - 6 * b1 * b2 * r1 * r2 + b2 ** 2 * (-1 + 3 * r2 ** 2)) - 
            2 * a2 * (3 * b1 ** 3 * m1 * r1 * (-1 + r1 ** 2) - b1 ** 2 * b2 * (2 * m1 + m2) * (-1 + 
                3 * r1 ** 2) * r2 - 3 * b2 ** 3 * m2 * r2 * (-1 + r2 ** 2) + b1 * b2 ** 2 * (m1 + 2 * m2) * 
               r1 * (-1 + 3 * r2 ** 2)) + 2 * a1 * (3 * b1 ** 3 * m1 * r1 * (-1 + r1 ** 2) - 
              b1 ** 2 * b2 * (2 * m1 + m2) * (-1 + 3 * r1 ** 2) * r2 - 3 * b2 ** 3 * m2 * r2 * (-1 + 
                r2 ** 2) + b1 * b2 ** 2 * (m1 + 2 * m2) * r1 * (-1 + 3 * r2 ** 2) + 
              a2 * (b1 ** 2 * (-1 + 3 * r1 ** 2) - 6 * b1 * b2 * r1 * r2 + b2 ** 2 * (-1 + 3 * r2 ** 2))) - 
            b1 ** 4 * s1 ** 2 + b1 ** 2 * b2 ** 2 * s1 ** 2 + b1 ** 4 * r1 ** 2 * s1 ** 2 - 2 * b1 ** 3 * b2 * r1 * r2 * 
             s1 ** 2 + b1 ** 2 * b2 ** 2 * r2 ** 2 * s1 ** 2 + b1 ** 2 * b2 ** 2 * s2 ** 2 - b2 ** 4 * s2 ** 2 + 
            b1 ** 2 * b2 ** 2 * r1 ** 2 * s2 ** 2 - 2 * b1 * b2 ** 3 * r1 * r2 * s2 ** 2 + b2 ** 4 * r2 ** 2 * s2 ** 2)
    
    q3 = 1000000 * -4 * (b1 ** 4 * m1 * (-1 + r1 ** 2) ** 2 - b1 ** 3 * r1 * (-1 + r1 ** 2) * 
             (a1 - a2 + b2 * (3 * m1 + m2) * r2) + b2 ** 3 * (-1 + r2 ** 2) * 
             ((a1 - a2) * r2 + b2 * m2 * (-1 + r2 ** 2)) + b1 * b2 ** 2 * r1 * 
             (a1 - 3 * a1 * r2 ** 2 - b2 * (m1 + 3 * m2) * r2 * (-1 + r2 ** 2) + 
              a2 * (-1 + 3 * r2 ** 2)) + b1 ** 2 * b2 * ((a1 - a2) * (-1 + 3 * r1 ** 2) * r2 + 
              b2 * (m1 + m2) * (-1 - r2 ** 2 + r1 ** 2 * (-1 + 3 * r2 ** 2))))
    
    q4 = 1000000 * (b1 ** 4 * (-1 + r1 ** 2) ** 2 - 4 * b1 ** 3 * b2 * r1 * (-1 + r1 ** 2) * r2 - 
            4 * b1 * b2 ** 3 * r1 * r2 * (-1 + r2 ** 2) + b2 ** 4 * (-1 + r2 ** 2) ** 2 + 
            2 * b1 ** 2 * b2 ** 2 * (-1 - r2 ** 2 + r1 ** 2 * (-1 + 3 * r2 ** 2)))
    
    term16 = (2 * q2 ** 3 + 27 * q3 ** 2 * q0 - 72 * q4 * q2 * q0 - 9 * q3 * q2 * q1 + 27 * q4 * q1 ** 2)
    term21 = (q2 ** 2 / 4 + 3 * q4 * q0 - 3 * q3 * q1 / 4)
    term1sq = -256 * term21 ** 3 + term16 ** 2
    term1 = sqrt(term1sq+0*1j)  # Note use of complex arithmetic in R
    term23 = (term16 + term1) ** (1/3)
    term22 = 3 * q4 * term23
    
    temp1 = (4 * 2 ** (1 / 3) * term21)
    temp2 = (3 * 2 ** (1 / 3) * q4)
    temp3 = q3 ** 2 / (4 * q4 ** 2) - (2 * q2) / (3 * q4)
    temp4 = temp1 / term22 + term23 / temp2 
    rr = sqrt(temp3 + temp4) 
    temp5 = q3 ** 2 / (2 * q4 ** 2) - (4 * q2) / (3 * q4)
    temp6 = (-q3 ** 3 / 4 + q4 * q3 * q2 - 2 * q4 ** 2 * q1) / (q4 ** 3)
    ee = q3 ** 2 / (2 * q4 ** 2) - (4 * q2) / (3 * q4) - (4 * 2 ** (1 / 3) * term21) / term22 - term23 / (3 * 2 ** (1 / 3) * q4) -          (-q3 ** 3 / 4 + q4 * q3 * q2 - 2 * q4 ** 2 * q1) / (q4 ** 3 * rr) 

    dd = q3 ** 2 / (2 * q4 ** 2) - (4 * q2) / (3 * q4) - (4 * 2 ** (1 / 3) * term21) / term22 - term23 / (3 * 2 ** (1 / 3) * q4) +          (-q3 ** 3 / 4 + q4 * q3 * q2 - 2 * q4 ** 2 * q1) / (q4 ** 3 * rr) 
    temp7 = -q3 / (4 * q4) 
    
    # Potential roots are given by
    roots = [-q3 / (4 * q4) +rr / 2 + sqrt(dd) / 2,
             -q3 / (4 * q4) +rr / 2 - sqrt(dd) / 2,
             -q3 / (4 * q4) -rr / 2 + sqrt(ee) / 2,
             -q3 / (4 * q4) -rr / 2 - sqrt(ee) / 2]
    # Need to check these are really roots

    kr = np.array(roots) * (np.abs(np.imag(roots)) < 1e-10)
    def test(k):
        return (a1 + b1 * (r1 * (k - m1) + sqrt((k - m1) ** 2 + s1 ** 2))) -                (a2 + b2 * (r2 * (k - m2) + sqrt((k - m2) ** 2 + s2 ** 2)))
    num = np.where(np.abs(test(kr)) < 1e-10)  # Which potential root is actually a root?
    roots=np.sort(np.real(kr[num]))
    length=len(roots)
    crossedness=0
    
    if length>1: midpoints = (np.array(roots[:-1])+np.array(roots[1:]))/2 
    else: midpoints= np.array([])
    
    if length>0:
        sample_points = [roots[0] - 1]+ midpoints.tolist()+ [roots[-1] + 1]  # Choose some sensible sampling points
        sample_points = np.array(sample_points)
        svi_short = SVI(sviparam1, sample_points) 
        svi_long = SVI(sviparam2, sample_points) 
        crossedness = max(0, np.max(svi_short - svi_long))  # Maximal amount of crossing bounded below by zero
        
    return {"roots":roots,"crossedness":crossedness}


def sviSliceArbitrageCheck(sviMatrix):
    
    n=len(sviMatrix)
    
    def g(sviparams,k):
        a=sviparams.a
        b=sviparams.b
        sig=sviparams.sig
        rho=sviparams.rho
        m=sviparams.m

        discr = sqrt((k-m)**2 + sig**2);
        w = a + b *(rho*(k-m)+ discr);
        dw = b*rho + b *(k-m)/discr;
        d2w = b*sig**2/(discr*discr*discr);

        return(1 - k*dw/w + dw*dw/4*(-1/w+k*k/(w*w)-4) +d2w/2);

    arbitrageableSlices=[]
    for Slice in range(n):
        func = lambda x: g(sviMatrix[Slice],x)
        res = minimize(func, 1e-10, bounds=((-2,2),))
        if res.fun<0: 
            arbitrageableSlices.append(Slice)
    return arbitrageableSlices

def sviCalendarArbitrageCheck(sviMatrix):
    n=len(sviMatrix)
    arbtot = 0
    arbcheck = lambda i:SVI_ComputeRoots(sviMatrix[i],sviMatrix[i+1])['crossedness'] # Recall that ComputeSviRoots returns discriminants when there is a true real root.
    return sum(arbcheck(i) for i in range(n-1))

def sviSliceArbitrageCompute(sviMatrix):
    
    n=len(sviMatrix)
    
    def g(sviparams,k):
        a=sviparams.a
        b=sviparams.b
        sig=sviparams.sig
        rho=sviparams.rho
        m=sviparams.m

        discr = sqrt((k-m)**2 + sig**2);
        w = a + b *(rho*(k-m)+ discr);
        dw = b*rho + b *(k-m)/discr;
        d2w = b*sig**2/(discr*discr*discr);

        return(1 - k*dw/w + dw*dw/4*(-1/w+k*k/(w*w)-4) +d2w/2);

    sliceArb=[]
    for Slice in range(n):
        func = lambda x: g(sviMatrix[Slice],x)
        res = minimize(func, 1e-10, bounds=((-2,2),))
        kArb = res.x
        gArb = min(res.fun,0)
        sliceArb.append([Slice,kArb,gArb])
    return sliceArb

# Fit SVI independently to each slice
def sviFit(ivolData):
    bidVols=ivolData["Bid"]
    askVols=ivolData["Ask"]
    expDates=unique(ivolData["Texp"])
    sviMatrix=[]
    
    for sliceNum in range(len(expDates)):
        t = expDates[sliceNum]
        texp = ivolData["Texp"]
        bidVol=bidVols[texp==t]
        askVol=askVols[texp==t]
        pick=(bidVol>0)&(askVol>0)  #valid and positive vols
        midVar=(square(bidVol[pick])+square(askVol[pick]))/2
        fwd=unique(ivolData["Fwd"][texp==t])
        logstrike=log(ivolData["Strike"][texp==t]/fwd)[pick]
        sviGuess=np.array([mean(midVar),0.1,0.1,-0.7,0])
        a1,b1,sig1,rho1,m1=sviGuess
        def obj(sviparams):
            a,b,sig,rho,m=sviparams
            sviVar=SVI(SVIPARAMS(a,b,sig,rho,m),logstrike)
            return nansum(square(subtract(midVar,sviVar)))
        bounds=[(-1000,+1000), (0,100), (.00000001,100), (-.999,+.999), (-10,+10)]
        res=minimize(obj, sviGuess, method='L-BFGS-B',bounds=bounds,options={"maxiter":100})
        sviMatrix.append(res.x * np.array([t,t,1,1,1]))
    return pd.DataFrame(sviMatrix,columns=["a","b","sig","rho","m"])


# Fit the SVI-Sqrt parametrization
# This version gets rid of alpha and fits only slices beyond a certain cutoff expiration.
# Other slices are obtained by projection
class SVISQRTPARAMS:
    def __init__(self, rho, eta):
        self.rho=rho
        self.eta=eta

def sviSqrt(sviSqrtParams,k,w0):
    rho = sviSqrtParams.rho
    eta = sviSqrtParams.eta
    w = w0/2*(1+rho*eta/sqrt(w0)*k+sqrt((eta/sqrt(w0)*k+rho)**2+1-rho**2))
    return w

# Compute w0 from implied vol smiles
def computeW0(ivolData):

    bidVols=ivolData["Bid"]
    askVols=ivolData["Ask"]
    expDates=unique(ivolData["Texp"])
    nSlices=len(expDates)
    w0 = {}
    
    for sliceNum in range(nSlices):
        t = expDates[sliceNum]
        texp = ivolData["Texp"]
        bidVol=bidVols[texp==t]
        askVol=askVols[texp==t]
        pick = (bidVol>0)&(askVol>0)&(bidVol<askVol)
        midVar = (bidVol[pick]**2+askVol[pick]**2)/2; midVar=midVar.to_numpy()
        fwd = unique(ivolData["Fwd"][texp==t])
        logstrike = log(ivolData["Strike"][texp==t]/fwd)[pick]; logstrike=logstrike.to_numpy()
        w0[str(sliceNum)] = t*stinterp(logstrike,midVar,np.array([0]))["y"]
    return w0

def sviSqrtFit(ivolData):
    bidVols=ivolData["Bid"]
    askVols=ivolData["Ask"]
    expDates=unique(ivolData["Texp"])
    nSlices=len(expDates)
    sviMatrix=[]
    
    nrows = ivolData.shape[0]
    midV = empty(nrows); midV[:] = np.nan
    kk = empty(nrows); kk[:] = np.nan
    ww0 = empty(nrows); ww0[:] = np.nan
    
    # Compute w0, keeping k and midVar as we go
    for sliceNum in range(nSlices):
        t = expDates[sliceNum]
        texp = ivolData["Texp"]
        bidVol = bidVols[texp==t];
        askVol = askVols[texp==t];
        pick = (bidVol>0)&(askVol>0)&(bidVol<askVol)
        midVar = (bidVol[pick]**2+askVol[pick]**2)/2; midVar=midVar.to_numpy()
        fwd = unique(ivolData["Fwd"][texp==t])
        logstrike = log(ivolData["Strike"][texp==t]/fwd)[pick]; logstrike=logstrike.to_numpy()
        w0 = t*stinterp(logstrike,midVar,np.array([0]))["y"]
        # Now put in correct place in columns
        ww0[(texp==t)&pick]= w0
        midV[(texp==t)&pick]= midVar
        kk[(texp==t)&pick]= logstrike
        
    tcutoff = min(0.5,max(expDates))
    
    # Define objective function
    def obj(inputParam):
        param=SVISQRTPARAMS(inputParam[0],inputParam[1])
        sviSqrtVar = sviSqrt(param,kk,ww0)/texp
        distance=square(subtract(midV,sviSqrtVar))
        tmp = nansum(distance[texp >= tcutoff])
        return tmp
    
    sviSqrtGuess=[-0.7,1.0] # rho: -0.7, eta: 1.0
    
    bounds=[(-.999,+.999), (-np.inf,+np.inf)]
    res=minimize(obj, [-0.7,1.0], method='L-BFGS-B',bounds=bounds,options={"maxiter":100})
    rho,eta=res.x
    # Now convert the result to an SVI matrix
    sel = ~isnan(ww0)
    w0r = unique(ww0[sel])
    rho = np.array([rho]*nSlices)
    a = w0r/2*(1-square(rho))
    gg = eta/sqrt(w0r)
    b = w0r/2*gg
    m = -rho/gg
    sig = sqrt(1-square(rho))/gg
    
    sviMatrix=[[a[i],b[i],sig[i],rho[i],m[i]] for i in range(len(a))]
        
    return pd.DataFrame(sviMatrix,columns=["a","b","sig","rho","m"])


pnorm = lambda x : norm.cdf(x)
def BSFormula(S0, K, T, r, sigma):
    x = log(S0/K)+r*T;
    sig = sigma*sqrt(T);
    d1 = x/sig+sig/2;
    d2 = d1 - sig;
    pv = exp(-r*T);
    return S0*pnorm(d1) - pv*K*pnorm(d2)


def sviFitQR(ivolData,sviGuess,penaltyFactor=100):
    callVals=ivolData["CallMid"]
    bidVols=ivolData["Bid"]
    askVols=ivolData["Ask"]
    midVols=(bidVols+askVols)/2
    volSpreads=(askVols-bidVols)/2
    expDates=unique(ivolData["Texp"])
    nSlices=len(expDates)
    sviGuess=sviGuess.to_numpy()
    sviMatrix=sviGuess
    
    nrows = ivolData.shape[0]
    midV = empty(nrows); midV[:] = np.nan
    kk = empty(nrows); kk[:] = np.nan
    ww0 = empty(nrows); ww0[:] = np.nan


    ######################################
    #for (slice in rev(slices)){  # Start from the latest slice
    for sliceNum in range(nSlices): # Start from the earliest slice
        t = expDates[sliceNum]
        texp = ivolData["Texp"]
        midVal = callVals[texp==t]
        pick=~isnan(midVal)
        midVal = midVal[pick]
        midVols1 = midVols[(texp==t)&pick]
        volSpreads1 = volSpreads[(texp==t)&pick]
        fwd = unique(ivolData["Fwd"][texp==t])
        logstrike = log(ivolData["Strike"][texp==t]/fwd)[pick]; logstrike=logstrike.to_numpy()
        
        def sqDist(sviparams): # square distance, simple
            a,b,sig,rho,m=sviparams
            sviVar=SVI(SVIPARAMS(a,b,sig,rho,m),logstrike)/t # Convert to variance-style
            outVal=BSFormula(fwd, fwd*exp(logstrike), t, 0, sqrt(abs(sviVar)))
            return nansum(square(subtract(midVal,outVal)))
        
        def sqDistOld(sviparams): # square distance, weighted by spread
            a,b,sig,rho,m=sviparams
            sviVar=SVI(SVIPARAMS(a,b,sig,rho,m),logstrike)/t # Convert to variance-style
            sviVol=sqrt(abs(sviVar))
            distance=square(subtract(midVols1,sviVol))
            distance=distance/square(volSpreads1)
            return nansum(distance)
        
        def sqDistN(sviparams): # Normalized squared distance
            return sqDist(sviparams)/sqDist(sviGuess[sliceNum,:])
        
        def crossPenalty(sviparams):
            a,b,sig,rho,m=sviparams
            cPenalty = 0
            if sliceNum >= 1: # Compare with previous slice
                slicePrev=sviMatrix[sliceNum-1,:]
                cPenalty = SVI_ComputeRoots(slicePrev, sviparams)["crossedness"]
            else: # Ensure no negative variances on first slice
                minVar = a+b*sig*sqrt(abs(1-rho**2))
                negVarPenalty = min(100,exp(-1/minVar))
                cPenalty = negVarPenalty
            
            if sliceNum < nSlices-1:
                sliceNext = sviMatrix[sliceNum+1,:]
                cPenalty = cPenalty + SVI_ComputeRoots(sliceNext, sviparams)["crossedness"]
            return cPenalty*penaltyFactor
        
        # Compute objective function
        def obj(sviparams):
            return sqDistN(sviparams)+crossPenalty(sviparams)    
            
        # Optimize
        bounds=[(-np.inf,+np.inf), (0,+np.inf), (0,+np.inf), (-1,+np.inf), (-np.inf,+np.inf)]
        res=minimize(obj, sviGuess[sliceNum,:], method='L-BFGS-B',bounds=bounds,options={"maxiter":100})
        sviMatrix[sliceNum,:] = res.x
        #if(abs(fit$par["rho"])>1){ #Only send to L-BFGS-B if rho is outside permitted range
        #    fit <- optim(sviGuess[slice,],obj,method="L-BFGS-B",lower=c(-1000,0,0.00000001,-.999,-10),upper=c(+1000,100,100,+.999,+10));
    
    return pd.DataFrame(sviMatrix,columns=["a","b","sig","rho","m"])


# ## 5 FIT surface
# Function to compute total variance given k and t
# texp is a vector of times to expiration
def sviW(sviMatrix,texp,k,t):
    if not isinstance(k, np.ndarray):
        k=np.array([k])
    if isinstance(t, np.ndarray) and len(t)!=1:
        print("t should be a scalar value")
        raise
    if not isinstance(t, np.ndarray):
        t=np.array([t])
    # Vector of SVI variance for a given strike k
    def sviWk(k):
        m = sviMatrix.shape[0]
        func = lambda i: SVI(sviMatrix.iloc[i,:],k)
        return vectorize(func)(np.arange(m))

    # Function to compute interpolated variance for any strike and expiration
    def wInterp(k,t):
        return stinterp(texp,sviWk(k),t)["y"]

    # Vectorized function that returns implied total variance for vectors k and t
    func=lambda k1: wInterp(k1,t)
    return vectorize(func)(k)
    # End of sviW
    


def plotSVI(sviMatrix,xrange=None,yrange=None):
    ks=np.linspace(-1.0,1.0,1000)
    for idx,row in sviMatrix.iterrows():
        sviparams=SVIPARAMS.load(row)
        sviV=np.vectorize(lambda k:SVI(sviparams,k))
        plt.plot(ks,sviV(ks))
        
# plotIvols
# plotTotalVar
# plotSkews
# plotLogSkews

def plot3dSVI(sviMatrix,texp):
    def volTVS(k,t):
        return sqrt(sviW(sviMatrix,texp,k,t)/t)
    
    logstrikes=linspace(-0.5,0.5,40)
    times=np.flip(linspace(0.04,1.74,40))
    X,Y=np.meshgrid(times,logstrikes)
    vols=np.zeros((len(logstrikes),len(times)))
    for i, t in enumerate(times):
        vols[:,i]=volTVS(logstrikes,t)


    # setup the figure and axes
    fig = plt.figure(figsize=(15, 40))
    ax1 = fig.add_subplot(311, projection='3d')
    #ax2 = fig.add_subplot(312, projection='3d')
    #ax3 = fig.add_subplot(313, projection='3d')

    ax1.plot_surface(X, Y, vols, rstride=1, cstride=1,cmap='twilight', edgecolor='none')
    ax1.set_xlabel("expiration")
    ax1.set_ylabel("log strike")
    ax1.set_zlabel("impvol")
    ax1.invert_xaxis()
    ax1.view_init(30, 30)

    #ax2.plot_surface(X, Y, vols, rstride=1, cstride=1,cmap='twilight', edgecolor='none')
    #ax2.set_title('surface')
    #ax2.view_init(30, -80)

    #ax3.plot_surface(X, Y, vols, rstride=1, cstride=1,cmap='twilight', edgecolor='none')
    #ax3.set_title('surface')
    #ax3.view_init(30, 180)
    plt.show()

