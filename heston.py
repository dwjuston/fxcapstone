#!/usr/bin/env python
# coding: utf-8

# In[13]:


import numpy as np
from blackscholes import BSImpliedVolOTM
from numpy import log, exp, sqrt, tanh, square, subtract
from math import pi
from scipy.integrate import quad
from scipy.stats import norm
from scipy.optimize import least_squares

# --------- Heston ------------------
def phiHeston(lbd, rho, eta, vbar, v):
    def inner(u, t):
        al = -u*u/2 - 1j*u/2;
        bet = lbd - rho*eta*1j*u;
        gam = eta**2/2;
        d = sqrt(bet*bet - 4*al*gam);
        rp = (bet + d)/(2*gam);
        rm = (bet - d)/(2*gam);
        g = rm / rp;
        D = rm * (1 - exp(-d*t))/ (1 - g*exp(-d*t));
        C = lbd * (rm * t - 2/(eta**2) * log( (1 - g*exp(-(d*t)))/(1 - g) ) );
        return(exp(C*vbar + D*v));
    return inner

def OptionOTM(phi,k,t):
    integrand = lambda u : np.real(exp(-1j*u*k)*phi(u - 1j/2, t)/(u**2 + 1/4))
    def integrate(func, a, b):
        real_func = lambda x : np.real(func(x))
        return quad(real_func, a, b)[0]
    res = exp(min(k,0)) - exp(k/2)/pi*integrate(integrand, 0, np.inf)
    return res

def impvolPhi(phi):
    def inner(k,t):
        return BSImpliedVolOTM(1, exp(k), t, 0, OptionOTM(phi,k,t))
    return inner


# In[14]:


'''
print("Heston")
phi=phiHeston(lbd=0.6067, rho=-0.7571, eta=0.2928, vbar=0.0707, v=0.0654)
k=0
t=1
price=OptionOTM(phi, k, t)
print('price',price)
vol=BSImpliedVolOTM(1, exp(k), t, 0, price)
print('impvol',vol)
'''


# In[28]:


def calibrate_Heston(ks, vols_mkt, t, guess):
    def inner(five):
        x1,x2,x3,x4,x5=five
        vbar=exp(x1)
        rho=tanh(x2)
        eta=exp(x3)
        lbd=exp(x4)
        theta=exp(x5)
        phi=phiHeston(lbd=lbd, rho=rho, eta=eta, vbar=vbar, v=theta)
        # TODO: multiple time slices fitted together; glue each t's result together
        impVols=np.array([impvolPhi(phi)(k,t) for k in ks]).flatten()
        return square(subtract(impVols,vols_mkt))
    
    res=least_squares(inner, guess, method='lm').x
    
    vbar=exp(res[0])
    rho=tanh(res[1])
    eta=exp(res[2])
    lbd=exp(res[3])
    theta=exp(res[4])
    
    phi=phiHeston(lbd=lbd, rho=rho, eta=eta, vbar=vbar, v=theta)
    return phi, vbar,rho,eta,lbd,theta

K_A = lambda F, vol, T : F * exp(vol**2 * T * 0.5)
K_C = lambda F, vol, T, delta, Q: F * exp(vol ** 2 * T / 2 - vol * sqrt(T) * norm.ppf(delta * exp(Q*T)))
K_P = lambda F, vol, T, delta, Q: F * exp(vol ** 2 * T / 2 + vol * sqrt(T) * norm.ppf(delta * exp(Q*T)))

k_A = lambda vol, T : vol**2 * T * 0.5
k_C = lambda vol, T, delta, Q : vol ** 2 * T / 2 - vol * sqrt(T) * norm.ppf(delta * exp(Q*T))
k_P = lambda vol, T, delta, Q : vol ** 2 * T / 2 + vol * sqrt(T) * norm.ppf(delta * exp(Q*T))


# In[29]:


'''
vols_mkt=np.array([0.079,0.0775,0.08,0.0875,0.097])
T=0.5
Q=0
ks = [k_P(vols_mkt[0],T,0.1,Q), 
      k_P(vols_mkt[1],T,0.25,Q), 
      k_A(vols_mkt[2],T), 
      k_C(vols_mkt[3],T,0.25,Q), 
      k_C(vols_mkt[4],T,0.1,Q)]
guess=np.array([log(0.01), 0, log(0.05), log(10), log(0.08)])

calibrate_Heston(ks, vols_mkt, T, guess)
'''


# In[ ]:




